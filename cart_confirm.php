<?php
require __DIR__. '/__db_connect.php';
$pageName = 'cart_confirm';


if(isset($_SESSION['user']) and !empty($_SESSION['cart'])) {

    // *** 取得購物車商品資料 begin
    $data = [];
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM products WHERE sid IN (%s)", implode(',', $keys));
    $rs = $mysqli->query($sql);
    while ($r = $rs->fetch_assoc()) {
        $r['qty'] = $_SESSION['cart'][$r['sid']];

        $data[$r['sid']] = $r;
    }
    // *** 取得購物車商品資料 end

    $total_price = 0;
    foreach($data as $k=>$v){
        $total_price += $v['price']*$v['qty'];
    }


    $o_sql = "INSERT INTO `orders`(`member_sid`, `amount`, `order_date`) VALUES (?, ?, NOW())";
    $o_stmt = $mysqli->prepare($o_sql);

//    if($mysqli->error){
//        echo $mysqli->error. '<br>';
//    }

    $o_stmt->bind_param('ii',
        $_SESSION['user']['id'],
        $total_price
    );


    $o_stmt->execute();

//    if($o_stmt->error){
//        echo $o_stmt->error. '<br>';
//    }

    if($o_stmt->affected_rows==1){
        $order_sid = $mysqli->insert_id; //取得最近新增資料的主鍵

        $od_sql = "INSERT INTO `order_details`(
                    `order_sid`, `product_sid`, `price`, `quantity`
                    ) VALUES (?,?,?,?)";
        $od_stmt = $mysqli->prepare($od_sql);

        foreach($keys as $k) {
            $od_stmt->bind_param('iiii',
                $order_sid,
                $k,
                $data[$k]['price'],
                $data[$k]['qty']
            );

            $od_stmt->execute();
        }
        unset($_SESSION['cart']);

    } else {
        // 錯誤處理
    }


}
//echo isset($order_sid) ? '訂購成功' : '失敗';
?>
<?php include __DIR__. '/__html_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>

<div class="container">
    <?php if(isset($order_sid)): ?>
        <div class="alert alert-primary">
            訂購成功
        </div>
    <?php else: ?>
        <div class="alert alert-danger">
            訂購失敗
        </div>
    <?php endif ?>
</div>

</div>
<?php include __DIR__. '/__html_foot.php' ?>
