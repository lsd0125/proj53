<?php
date_default_timezone_set('Asia/Taipei');
require __DIR__. '/__db_connect.php';
$pageName = 'history';


if(isset($_SESSION['user'])) {
    $ddate = date("Y-m-d", time()-6*30*24*60*60);


    $o_sql = sprintf("SELECT * FROM `orders` WHERE `member_sid`=%s AND order_date>'%s'",
        $_SESSION['user']['id'],
        $ddate
    );
    $o_rs = $mysqli->query($o_sql);
    $o_ar = [];

    while($r=$o_rs->fetch_assoc()){
        $o_ar[$r['sid']] = $r;
    }

    $o_keys = array_keys($o_ar); // 取得訂單的所有 sid

    $od_sql = sprintf("SELECT od.*, p.bookname, p.book_id 
                FROM `order_details` od 
                JOIN `products` p 
                ON od.`product_sid`=p.sid
                WHERE od.order_sid IN (%s)
                ", implode(',', $o_keys));

    $od_rs = $mysqli->query($od_sql);
    $od_ar = [];
    while($r = $od_rs->fetch_assoc()){
        $od_ar[] = $r;
    }


} else {
    header('Location: ./');
    exit;
}
?>
<?php include __DIR__. '/__html_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>

    <div class="container">
        <?= $o_sql ?>
        <br>
        <?php print_r($o_ar) ?>
        <br>
        <?php print_r($od_ar) ?>
    </div>

</div>
<?php include __DIR__. '/__html_foot.php' ?>
