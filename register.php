<?php
$pageName = 'register';
?>
<?php include __DIR__. '/__html_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>

    <style>
        form>.form-group>small {
            color: red !important;
            display: none;
        }
    </style>
    <div class="row justify-content-md-center" style="margin-top: 20px" >

        <div class="col-md-6">

            <div id="info" class="alert" role="alert" style="display: none">
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="card-title">會員註冊</div>

                    <form name="form1" method="post" onsubmit="return checkForm()">
                        <div class="form-group">
                            <label for="nickname">暱稱 *</label>
                            <input type="text" class="form-control" id="nickname" name="nickname" placeholder="暱稱">
                            <small id="nicknameHelp" class="form-text text-muted">長度請大於兩個字元</small>
                        </div>
                        <div class="form-group">
                            <label for="email">電子郵箱 * (當作登入帳號)</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="電子郵箱">
                            <small id="emailHelp" class="form-text text-muted">請符合格式</small>
                        </div>
                        <div class="form-group">
                            <label for="password">密碼 *</label>
                            <input type="password" class="form-control" id="password" name="password">
                            <small id="passwordHelp" class="form-text text-muted">長度請大於六個字元</small>
                        </div>

                        <div class="form-group">
                            <label for="mobile">手機號碼</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="手機號碼">
                            <small id="mobileHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" placeholder="生日">
                            <small id="birthdayHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" name="address" id="address" cols="30" rows="5" placeholder="請填寫地址"></textarea>
                            <small id="addressHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>

                        <button type="submit" class="btn btn-primary" id="submit_btn">註冊</button>
                    </form>
                </div>


            </div>
        </div>
    </div>

</div>
    <script>
        function checkForm() {
            var nicknameHelp = $('#nicknameHelp'),
                emailHelp = $('#emailHelp'),
                passwordHelp = $('#passwordHelp'),
                submit_btn = $('#submit_btn');
            var emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
            var isPass = true;

            submit_btn.hide();
            nicknameHelp.hide();
            emailHelp.hide();
            passwordHelp.hide();
            $('#info').hide();

            if(form1.nickname.value.length < 2){
                nicknameHelp.show();
                isPass = false;
            }

            if(! emailPattern.test(form1.email.value)){
                emailHelp.show();
                isPass = false;
            }

            if(form1.password.value.length < 6){
                passwordHelp.show();
                isPass = false;
            }

            console.log( $(document.form1).serialize() );
            if(isPass) {

                $.post('register_api.php', $(document.form1).serialize(), function(data){


                    if(data.success){

                    } else {

                        submit_btn.show();
                    }

                    if(data.info){
                        var info = $('#info');
                        info.text(data.info.msg);
                        info.show();

                        info.attr('class', 'alert alert-'+data.info.type);
                    }
                }, 'json');


            }
            return false;
        }
    </script>
<?php include __DIR__. '/__html_foot.php' ?>